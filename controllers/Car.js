import Car from "../models/CarsModel.js";

export const getAllCar = async (req, res) => {
    try {
        const users = await Car.findAll({})
        res.json({
            status: true,
            data: [users]
        })
    } catch (error) {
        console.log(error);
    }
}

export const getCarById = async (req, res) => {
    let id = req.params.id
    let car = await Car.findOne({
        where: {
            id: id
        },
    })
    res.json({
        status: true,
        data: [car]
    })
}

export const postCar = async (req, res, next) => {
    const {
        plate, manufacture, model,image, rentperDay, capacity, description, availableAt, transmission, available, type, year, options, specs
     } = req.body
    try {
        Car.create({
            plate, manufacture, model,image, rentperDay, capacity, description, availableAt, transmission, available, type, year, options, specs
        }) 
        res.status(200).json({msg : 'Data berhasil di input'})
    } catch (error) {
        console.log(error);
    }
    next()
}

export const updateCar= async (req, res) => {
    let id = req.params.id
    let car = await Car.findOne({
        where: {
            id: id
        },
    })
    try {
        await Car.update(req.body, {
            where: {
                id: id
            }
        })
        res.status(200).json([car])
    } catch (error) {
        console.log(error);
    }
}

export const deleteCar = async (req, res) => {
    let id = req.params.id
    Car.destroy({
        where: {
            id: id
        },
    });
    res.json({
        msg: "Data terhapus"
    })
}