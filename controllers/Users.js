import Users from "../models/UsersModel.js";
import bcrypt from "bcrypt"
import jwt from "jsonwebtoken";

export const getUsers = async (req, res) => {
    try {
        const users = await Users.findAll({
            attributes: ['id', 'name', 'email', 'role']
        })
        res.json({
            status: true,
            data: [users]
        })
    } catch (error) {
        console.log(error);
    }
}

export const Register = async (req, res) => {
    const {
        name,
        email,
        password,
        confPassword
    } = req.body
    if (password !== confPassword) return res.status(400).json({
        msg: "Password dan confirm Password tidak cocok"
    })
    const salt = await bcrypt.genSalt()
    const hashPassword = await bcrypt.hash(password, salt)
    try {
        await Users.create({
            name: name,
            email: email,
            role: "member",
            password: hashPassword
        })
        res.json({
            msg: "Register berhasil"
        })
    } catch (error) {
        console.log(error);
    }
}

export const Login = async (req, res) => {
    try {
        const user = await Users.findAll({
            where: {
                email: req.body.email
            }
        })
        const match = await bcrypt.compare(req.body.password, user[0].password)
        if (!match) return res.status(400).json({
            msg: "Wrong Password"
        })
        const userId = user[0].id
        const name = user[0].name
        const email = user[0].email
        const role = user[0].role
        const accessToken = jwt.sign({
            userId,
            name,
            email,
            role
        }, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: '300s'
        })
        const refreshToken = jwt.sign({
            userId,
            name,
            email,
        }, process.env.REFRESH_TOKEN_SECRET, {
            expiresIn: '1d'
        })
        await Users.update({
            refresh_token: refreshToken
        }, {
            where: {
                id: userId
            }
        })
        res.cookie('refreshToken', refreshToken, {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000,
        })
        res.json({
            msg: "Anda Berhasil Login!",
            accessToken
        })
    } catch (error) {
        res.status(404).json({
            msg: "Email Tidak Ditemukan"
        })
    }
}

export const CurrentUsers = async (req, res) => {
    const refreshToken = req.cookies.refreshToken
    let user = await Users.findOne({where: {refresh_token: refreshToken}})
    res.json([user])
}


export const Logout = async (req, res) => {
    const refreshToken = req.cookies.refreshToken
    if (!refreshToken) return res.sendStatus(204)
    const user = await Users.findAll({
        where: {
            refresh_token: refreshToken
        }
    })
    if (!user[0]) return res.sendStatus(204)
    const userId = user[0].id
    await Users.update({
        refresh_token: null
    }, {
        where: {
            id: userId
        }
    })
    res.clearCookie('refreshToken')
    return res.status(200).json({msg: "Anda Berhasil Logout!"})
}