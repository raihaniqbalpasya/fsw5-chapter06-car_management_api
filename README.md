# FSW5-Chapter06-Car_Management_API

## Link Open API
http://localhost:5001/api-docs/

## Endpoint

### User Auth
1. Register Account = POST http://localhost:5001/users/register
2. Login Account = POST http://localhost:5001/users/login
3. Get current data User = GET http://localhost:5001/users/current
4. Refresh token = GET http://localhost:5001/token
5. Logout = DELETE http://localhost:5001/logout

### Cars CRUD
1. Get all cars data = GET http://localhost:5001/cars
2. Get car data by Id = GET http://localhost:5001/cars/:id
3. Add new car data = POST http://localhost:5001/car
4. Updated car data = PUT http://localhost:5001/car/:id
5. Delete car data = DELETE http://localhost:5001/car/:id

### Superadmin
1. Updated member to admin = PUT http://localhost:5001/superadmin/:id

Ket: Pada Challenge Chapter 6 ini, kami dari Kelompok 4 belum dapat membuat role permission untuk member, admin, maupun superadmin, sehingga pada projek ini role hanya sebagai String biasa dan dapat diubah melalui endpoint superadmin diatas.
