import express  from "express";
import { getUsers, Login, Logout, Register, CurrentUsers } from "../controllers/Users.js";
import { verifyToken } from "../middleware/verifyToken.js";
import { refreshToken } from "../controllers/refreshToken.js";
import { deleteCar, getAllCar, getCarById, postCar, updateCar } from "../controllers/Car.js";
import { updateMemberToAdmin } from "../controllers/Superadmin.js";

const router = express.Router()

//Authentication route
router.post('/users/register', Register)
router.post('/users/login', Login)
router.get('/token', refreshToken)
router.delete('/logout', Logout)

//Superadmin & Admin route
router.put('/superadmin/:id',verifyToken, updateMemberToAdmin)
router.post('/car',verifyToken, postCar)
router.put('/car/:id', verifyToken, updateCar)
router.delete('/car/:id', verifyToken, deleteCar)

// Users route
router.get('/users', getUsers)
router.get('/users/current',verifyToken, CurrentUsers )
router.get('/cars/:id', verifyToken, getCarById)
router.get('/cars', verifyToken,getAllCar)


export default router