import {Sequelize} from "sequelize";
import db from "../config/database.js";

const { DataTypes } = Sequelize

const Car = db.define('car', {
    plate:{
        type: DataTypes.STRING
    },
    manufacture:{
        type: DataTypes.STRING
    },
    model:{
        type: DataTypes.STRING
    },
    image:{
        type: DataTypes.STRING
    },
    rentperDay:{
        type: DataTypes.STRING
    },
    capacity:{
        type: DataTypes.STRING
    },
    description:{
        type: DataTypes.STRING
    },
    availableAt:{
        type: DataTypes.STRING
    },
    transmission:{
        type: DataTypes.STRING
    },
    available : {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
    },
    type:{
        type: DataTypes.STRING
    },
    year:{
        type: DataTypes.STRING
    },
    options : {
        type: DataTypes.STRING
    },
    specs : {
        type: DataTypes.STRING
    }
}, {
    freezeTableName: true
})

export default Car