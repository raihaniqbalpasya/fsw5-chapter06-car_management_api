import express from "express"
import dotenv from "dotenv";
import cookieParser from "cookie-parser";
import cors from "cors"
import db from "./config/database.js";
import router from "./routes/index.js";
import Cars from "./models/CarsModel.js";
import Users from "./models/UsersModel.js";
dotenv.config()
const app = express()
const PORT = 5001;

//Swagger
import swaggerUi from "swagger-ui-express";
import apiDocumentation from "./apidocs.json" assert { type: "json"};

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(apiDocumentation));

try {
    await db.authenticate()
    console.log('Database connected');
    await Cars.sync()
    await Users.sync()
} catch (error) {
    console.error(error);
}

app.get('/', (req,res) => {
    res.send('hai');
})

app.use(cors({
    credentials: true,
    origin: 'http://localhost:3000'
}))
app.use(cookieParser())
app.use(express.json())
app.use(router)

app.listen(PORT, () => console.log(`Server Running at http://localhost:${PORT}`));
